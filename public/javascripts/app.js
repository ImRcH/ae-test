const API_URL = 'http://localhost:3000/api';
const App = {
    getRequest(url) {
        return new Promise((resolve, reject) => {
            const xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() {
                if (xmlHttp.readyState === 4) {
                    if (xmlHttp.status === 200) {
                        return resolve(JSON.parse(xmlHttp.responseText));
                    }
                    reject(xmlHttp.responseText);
                }
            };
            xmlHttp.open('GET', url, true);
            xmlHttp.send(null);
        });
    },
    getTransactions() {
        return this.getRequest(`${API_URL}/transactions`)
            .then(response => (response.data && response.data.length ? response.data : {}))
            .catch(error => console.error(error));
    },
    buildHTML(transactionsCollection) {
        const collectionLength = transactionsCollection.length;
        const containerEl = document.getElementById('container');
        if (collectionLength) {
            const ul = document.createElement('ul');
            ul.setAttribute('id', 'transactions-list');
            for (let i = 0; i < collectionLength; i++) {
                const li = document.createElement('li');
                const { type, amount, effectiveDate, externalId, _id, createdDate } = transactionsCollection[i];
                li.setAttribute('class', 'item closed');
                li.innerHTML = `<div class="item-block item-${type}">
                    <span class="item-bold">${_id}</span> - <span class="item-amount">${amount}</span> <span class="item-type">(${type})</span>
                    </div><div class="item-details">
                        <ul class="list-details">
                            <li class="details-item"><span>Internal ID:</span> ${_id}</li>
                            <li class="details-item"><span>ID:</span> ${externalId}</li>
                            <li class="details-item"><span>Type:</span> ${type}</li>
                            <li class="details-item"><span>Effective Date:</span> ${effectiveDate}</li>
                            <li class="details-item"><span>Created Date:</span> ${createdDate}</li>
                        </ul>
                    </div>`;
                ul.appendChild(li);
            }
            containerEl.appendChild(ul);
        } else {
            containerEl.innerText = 'No data to show ...';
        }
    },
    bindDOMEvents() {
        const blocks = document.querySelectorAll('.item');
        for (let i = 0; i < blocks.length; i++) {
            blocks[i].onclick = () => {
                if (blocks[i].classList.contains('closed')) {
                    blocks[i].classList.remove('closed');
                    blocks[i].classList.add('open');
                } else {
                    blocks[i].classList.remove('open');
                    blocks[i].classList.add('closed');
                }
            };
        }
    }
};

document.addEventListener('DOMContentLoaded', () => {
    App.getTransactions()
        .then(transactions => App.buildHTML(transactions))
        .then(() => App.bindDOMEvents());
});
