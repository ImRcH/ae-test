import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import Debug from 'debug';
import express from 'express';
import logger from 'morgan';
import sassMiddleware from 'node-sass-middleware';
import path from 'path';

import index from './routes';
import transactions from './routes/api/transactions';
import { STATUS_CODE_INTERNAL_SERVER_ERROR, STATUS_CODE_NOT_FOUND } from './src/http/statusCodes';

const app = express();
const debug = Debug('test:app');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(
    sassMiddleware({
        src: path.join(__dirname, 'public'),
        dest: path.join(__dirname, 'public'),
        indentedSyntax: true,
        sourceMap: true
    })
);
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/api', transactions);

app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = STATUS_CODE_NOT_FOUND;
    next(err);
});

/* eslint no-unused-vars: 0 */
app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || STATUS_CODE_INTERNAL_SERVER_ERROR);
    res.render('error');
});

process.on('uncaughtException', (err) => {
    debug('Caught exception: %j', err);
    process.exit(1);
});

export default app;
