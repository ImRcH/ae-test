# Fullstack Developer Test Task

## Requirements

 - Node.js ~10.0.0
 - Npm ~6.1.0

### Start Local Web Server

    $ npm i
    $ npm start


## REST API

### Local configuration

 - host: **localhost**
 - port: **3000**

### Get transactions list

**Request:**

GET http://{{host}}:{{port}}/api/transactions

**Response:**

    {
        "code": 200,
        "data": [
            {
              "externalId": "ebe08fd5-777e-4b2d-85e4-ec756d8e5b67",
              "type": "credit",
              "amount": 10.23,
              "effectiveDate": "2018-07-10T14:04:20.000Z",
              "createdDate": "2018-07-12T07:53:19.305Z",
              "_id": "cIaT7EhNL5CsLn5m"
            }
        ]
    }

### Create transaction

**Request:**

POST http://{{host}}:{{port}}/api/transactions

Content-Type: application/json; charset=utf-8

    {
        "id": "ebe08fd5-777e-4b2d-85e4-ec756d8e5b67"
        "type": "debit"
        "amount": 20.2
        "effectiveDate": "Tue Jul 10 2018 17:04:20 GMT+0300"
    }

**Response:**

    {
        "code": 201,
        "data": {
            "externalId": "ebe08fd5-777e-4b2d-85e4-ec756d8e5b67",
            "type": "credit",
            "amount": 10.23,
            "effectiveDate": "2018-07-10T14:04:20.000Z",
            "createdDate": "2018-07-12T07:53:19.305Z",
            "_id": "cIaT7EhNL5CsLn5m"
        }
    }

## REST API Testing

For the REST API testing you can use `Postman` or `Jetbrains Editor-based Rest Client`
We can look at request examples:

```
./http
├── createTransaction.http
└── getTransactions.http
```