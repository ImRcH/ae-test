import { STATUS_CODE_CREATED } from '../statusCodes';

export default class CreatedResponse {
    constructor(createdModel) {
        this.model = createdModel;
    }

    get statuseCode() {
        return STATUS_CODE_CREATED;
    }

    get responseBody() {
        return {
            code: STATUS_CODE_CREATED,
            data: this.model
        };
    }
}
