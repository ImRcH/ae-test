import { STATUS_CODE_BAD_REQUEST } from '../statusCodes';

export default class BadRequestResponse {
    constructor(message) {
        this.message = message;
    }

    get statuseCode() {
        return STATUS_CODE_BAD_REQUEST;
    }

    get responseBody() {
        return {
            code: STATUS_CODE_BAD_REQUEST,
            message: `Error: ${this.message}`
        };
    }
}
