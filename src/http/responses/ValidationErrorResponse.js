import { STATUS_CODE_DATA_VALIDATION_FAILED } from '../statusCodes';

export default class ValidationErrorResponse {
    constructor(message) {
        this.message = message;
    }

    get statuseCode() {
        return STATUS_CODE_DATA_VALIDATION_FAILED;
    }

    get responseBody() {
        return {
            code: STATUS_CODE_DATA_VALIDATION_FAILED,
            message: `Validation Error: ${this.message}`
        };
    }
}
