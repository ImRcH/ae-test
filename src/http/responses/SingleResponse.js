import { STATUS_CODE_OK } from '../statusCodes';

export default class SingleResponse {
    constructor(model) {
        this.models = model;
    }
    
    get statuseCode() {
        return STATUS_CODE_OK;
    }
    
    get responseBody() {
        return {
            code: STATUS_CODE_OK,
            data: this.models
        };
    }
}
