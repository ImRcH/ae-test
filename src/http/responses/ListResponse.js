import { STATUS_CODE_OK } from '../statusCodes';

export default class ListResponse {
    constructor(models) {
        this.models = models;
    }

    get statuseCode() {
        return STATUS_CODE_OK;
    }

    get responseBody() {
        return {
            code: STATUS_CODE_OK,
            data: this.models
        };
    }
}
