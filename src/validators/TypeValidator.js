const TYPE_DEBIT = 'debit';
const TYPE_CREDIT = 'credit';

export default class TypeValidator {
    static get availableTypes() {
        return new Set([TYPE_DEBIT, TYPE_CREDIT]);
    }

    static get errorMessage() {
        return 'Transaction type is invalid.';
    }

    static isValid(payload) {
        return new Promise((resolve, reject) => {
            if (TypeValidator.availableTypes.has(payload)) {
                return resolve();
            }
            reject(TypeValidator.errorMessage);
        });
    }
}