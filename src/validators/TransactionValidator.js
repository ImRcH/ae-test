import TypeValidator from './TypeValidator';
import AmountValidator from './AmountValidator';

export default class TransactionValidator {
    static isValid(payload) {
        return new Promise((resolve, reject) => {
            const validators = [];
            if (payload.type) {
                validators.push(TypeValidator.isValid(payload.type));
            }
            if (payload.amount) {
                validators.push(AmountValidator.isValid(payload.amount));
            }
            Promise.all(validators)
                .then(() => resolve(payload))
                .catch(error => reject(error));
        });
    }
}
