export default class AmountValidator {
    static get errorMessage() {
        return 'Transaction amount is invalid.';
    }

    static isValid(payload) {
        return new Promise((resolve, reject) => {
            if (!isNaN(parseFloat(payload)) && isFinite(payload) && (payload < 0)) {
                return reject(AmountValidator.errorMessage);
            }
            resolve();
        });
    }
}
