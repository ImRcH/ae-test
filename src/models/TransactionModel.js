import Datastore from 'nedb';

const Storage = new Datastore();

export default class TransactionModel {
    create(payloads) {
        return new Promise((resolve, reject) => {
            const data = {
                externalId: payloads.id,
                type: payloads.type,
                amount: parseFloat(payloads.amount),
                effectiveDate: new Date(payloads.effectiveDate),
                createdDate: new Date()
            };
            Storage.insert(data, (error, model) => (error ? reject(error) : resolve(model)));
        });
    }

    fetchSingle(id) {
        return new Promise((resolve, reject) => {
            Storage.findOne({ externalId: id }, (error, model) => (error ? reject(error) : resolve(model)));
        });
    }

    fetchCollection() {
        return new Promise((resolve, reject) => {
            Storage.find({}, (error, models) => (error ? reject(error) : resolve(models)));
        });
    }
}
