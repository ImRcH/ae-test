import express from 'express';
import TransactionValidator from '../../src/validators/TransactionValidator';
import ValidationErrorResponse from '../../src/http/responses/ValidationErrorResponse';
import TransactionModel from '../../src/models/TransactionModel';
import CreatedResponse from '../../src/http/responses/CreatedResponse';
import ListResponse from '../../src/http/responses/ListResponse';
import SingleResponse from '../../src/http/responses/SingleResponse';
import BadRequestResponse from '../../src/http/responses/BadRequestResponse';

const router = express.Router();

router.post('/transactions', (req, res) => {
    TransactionValidator.isValid(req.body)
        .then((validPayloads) => {
            const model = new TransactionModel();
            model
                .create(validPayloads)
                .then((createdModel) => {
                    const createdResponse = new CreatedResponse(createdModel);
                    res.status(createdResponse.statuseCode);
                    res.json(createdResponse.responseBody);
                })
                .catch((errorMessage) => {
                    const validationErrorResponse = new BadRequestResponse(errorMessage);
                    res.status(validationErrorResponse.statuseCode);
                    res.json(validationErrorResponse.responseBody);
                });
        })
        .catch((errorMessage) => {
            const validationErrorResponse = new ValidationErrorResponse(errorMessage);
            res.status(validationErrorResponse.statuseCode);
            res.json(validationErrorResponse.responseBody);
        });
});

router.get('/transactions', (req, res) => {
    const transactionModel = new TransactionModel();
    transactionModel.fetchCollection().then((listModels) => {
        const listResponse = new ListResponse(listModels);
        res.status(listResponse.statuseCode);
        res.json(listResponse.responseBody);
    }).catch((error) => {
        const validationErrorResponse = new BadRequestResponse(error);
        res.status(validationErrorResponse.statuseCode);
        res.json(validationErrorResponse.responseBody);
    });
});

router.get('/transactions/:id', (req, res) => {
    const transactionModel = new TransactionModel();
    transactionModel.fetchSingle(req.params.id).then((singleModel) => {
        const listResponse = new SingleResponse(singleModel);
        res.status(listResponse.statuseCode);
        res.json(listResponse.responseBody);
    }).catch((error) => {
        const validationErrorResponse = new BadRequestResponse(error);
        res.status(validationErrorResponse.statuseCode);
        res.json(validationErrorResponse.responseBody);
    });
});

export default router;
